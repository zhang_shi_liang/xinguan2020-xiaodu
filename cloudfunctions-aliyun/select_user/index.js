'use strict';
const db = uniCloud.database()
exports.main = async (event, context) => {
	//event为客户端上传的参数
	console.log('客户端上传的参数event : ' + event)
	let res = {
		// code : '5000',
		success: false,
		message: '请联系管理员'
	};
	let e = 9; //默认查询记录上限
	var timestamp = (new Date()).valueOf(); //获取当前毫秒的时间戳
	//获取表
	const collection = db.collection('user')
	if (event.uid) {
		let checkUserInfo = await collection.where({
			_id: event.uid,
			token: event.token //去数据集验token是否正确
		}).get();
		if (checkUserInfo.data[0]) { //正确则继续验证时间是否有效
			let tok_tim = timestamp - checkUserInfo.data[0].token_time //token存在了多长时间
			if (tok_tim < 86400000) { //小于24小时,执行操作
				if (event.data.limits) { //如果客户端传过记录上限,修改默认值
					e = event.data.limits;
				}
				// 执行操作
				res = await collection.limit(e).orderBy("_id", "desc").get()
				//code码,正常
				res.code = '0000'
				//返回数据给客户端
				return res

			} else {
				res.code = '4001'
				//返回数据给客户端
				return res
			}
		} else {
			res.code = '4003'
			//返回数据给客户端
			return res
		}
	} else {
		res.code = '5000'
		//返回数据给客户端
		return res
	}

};
