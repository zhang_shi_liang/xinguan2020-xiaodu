// 获得消毒地址列表
'use strict';
const db = uniCloud.database()
const role = 0
exports.main = async (event, context) => {
	let { res, user } = await cheak(event)
	if (res.code <= 1111) {
		const select_location = await db.collection('location').where({
			locationAddress: event.data.locationAddress
		}).count()
		res.data = select_location
	}
	return res
}

async function cheak(e){
	var res = { "code":"0000", "data":{} }
	var user = await db.collection('user').where({
		'_id'	:	e.uid,
		'token'	:	e.token
	}).get()
	user = user.data[0];
	if(user){
		if(role != user.role){
			res.code  = 4003
		}
	}else{
		res.code  = 4001;
	}
	return {res,user}
}
